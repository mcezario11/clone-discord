import React, { useRef, useEffect} from 'react';

import { Container, Messages, InputWrapper, Input, InputIcon } from './styles';
import ChannelMessage, { Mention } from '../ChannelMessage/index';


const ChannelData: React.FC = () => {
  const messageRef = useRef() as React.MutableRefObject<HTMLDivElement>;
  useEffect(() => {
    const div = messageRef.current;
    if ( div) {
      div.scrollTop = div.scrollHeight;
    }
  },[messageRef])
  return (
      <Container>
          <Messages ref={messageRef}>
          {Array.from(Array(15).keys()).map((n) => (
          <ChannelMessage
            key={n}
            author="Matheus Cezário"
            date="23/06/2020"
            content="Aplicativo React"
          />
        ))}

        <ChannelMessage
          author="Fulano"
          date="21/06/2020"
          content={
            <>
              <Mention>@Ciclano</Mention>, mencionado
            </>
          }
          hasMention
          isBot
        />
          </Messages>

          <InputWrapper>
            <Input type="text" placeholder="Conversar em #chat-livre"/>
            <InputIcon/>
          </InputWrapper>
      </Container>
  );
}

export default ChannelData;